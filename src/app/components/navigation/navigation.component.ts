import { Component, OnInit } from '@angular/core';
import { NavigateService } from '../../services/navigate.service';
import { destinations } from '../../routes';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  dests = destinations; // for navigation
  constructor(
    private navigate: NavigateService
    ) { }

  ngOnInit(): void {
  }

  nav(dest: string): void {
    this.navigate.nav(dest);
  }
}
