import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit {
  messages: string[] = [];
  dormant = true;

  constructor() { }

  ngOnInit(): void {
    this.messages.push('This is a test message');
  }

  removeMsg(i: number): void {
    this.messages.splice(i, 1);
  }
  removeAllMsg(): void {
    this.messages = [];
  }
}
