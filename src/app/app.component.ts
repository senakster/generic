import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  darkTheme = false;
  title = 'Generic App';

  toggleTheme(): void {
    this.darkTheme = !this.darkTheme;
  }
}
